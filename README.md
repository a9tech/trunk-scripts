# Trunk Scripts

CLI and linting rules for Trunk projects.

Add this to your project's `devDependencies`:

```bash
yarn add -D https://bitbucket.org/a9tech/trunk-scripts.git
```

## Editor Configuration

#### Atom

Install the following plugins:
- [linter-eslint](https://atom.io/packages/linter-eslint)
- [linter-stylelint](https://atom.io/packages/linter-stylelint)
- [prettier-atom](https://atom.io/packages/prettier-atom) with ESLint Integration enabled.


#### VS Code

Install the following plugins:
- [vscode-eslint](https://github.com/Microsoft/vscode-eslint)
- [vscode-stylelint](https://github.com/shinnn/vscode-stylelint)
- [prettier-vscode](https://github.com/prettier/prettier-vscode)

Note: `prettier.eslintIntegration` and `prettier.stylelintIntegration` are enabled per project `.vscode/settings.json`.

## Usage

Typically you'll use this in your npm scripts (or package scripts):

```json
{
  "scripts": {
    "format": "trunk-scripts format \"src/**/*.js\"",
    "lint": "npm run lint:js && npm run lint:css",
    "lint:js": "trunk-scripts lint \"app/**/*.js\"",
    "lint:css": "trunk-scripts lint-css \"./app/**/styles.js\""
  }
}
```

## Commands

`lint [options] [glob]`

Run eslint across your code base.

`format [options] [glob]`

### Eslint

Extend this configuration by adding it to your `.eslintrc`:

```json
{
  "extends": "./node_modules/trunk-scripts/eslint.js"
}
```

If you're using this in a browser free environment, you will probably want to disable the browser compatibility rules.

```json
{
  "rules": {
    "compat/compat": "off"
  }
}
```

### Styelint

Extend this configuration by adding it to your `.stylelint`:

```json
{
  "extends": "./node_modules/trunk-scripts/stylelint.js"
}
```

Please read the styled-components [Interpolation Tagging](https://www.styled-components.com/docs/tooling#interpolation-tagging) document for more information on how to avoid unnecessary stylelint errors.
