const path = require('path');
const spawn = require('cross-spawn');
const yargsParser = require('yargs-parser');
const { hasPkgProp, resolveBin, hasFile } = require('../utils');

const args = process.argv.slice(2);
const parsedArgs = yargsParser(args);

const here = p => path.join(__dirname, p);
const hereRelative = p => here(p).replace(process.cwd(), '.');

const useBuiltinConfig =
  !args.includes('--config') &&
  !hasFile('.prettierrc') &&
  !hasFile('prettier.config.js') &&
  !hasPkgProp('prettierrc');

const config = useBuiltinConfig ? ['--config', hereRelative('../config/prettierrc.js')] : [];

const useBuiltinEslintConfig =
  !args.includes('--config') &&
  !hasFile('.eslintrc') &&
  !hasFile('eslint.config.js') &&
  !hasPkgProp('eslintrc');

const eslintConfig = useBuiltinEslintConfig
  ? ['--eslint-config-path', hereRelative('../config/eslintrc.js')]
  : [];

const useBuiltinIgnore = !args.includes('--ignore') && !hasFile('.prettierignore');
const ignore = useBuiltinIgnore ? ['--ignore', hereRelative('../config/prettierignore')] : [];

const write = args.includes('--no-write') ? [] : ['--write'];
const list = ['--list-different'];

// this ensures that when running format as a pre-commit hook and we get
// the full file path, we make that non-absolute so it is treated as a glob,
// This way the prettierignore will be applied
const relativeArgs = args.map(a => a.replace(`${process.cwd()}/`, ''));

const filesToApply = parsedArgs._.length ? [] : ['**/*.+(js|jsx|json|less|css|ts|tsx|md)'];

const result = spawn.sync(
  resolveBin('prettier-eslint-cli', { executable: 'prettier-eslint' }),
  [...config, ...eslintConfig, ...ignore, ...write, ...list, ...filesToApply].concat(relativeArgs),
  { stdio: 'inherit' }
);

process.exit(result.status);
